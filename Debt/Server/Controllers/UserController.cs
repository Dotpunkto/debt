using System.Collections.Generic;
using Debt.Models.Entities;
using Debt.Server.Models.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Server.Controllers
{
    [Route("api/[controller]/[action]")]
    public class UserController
    {
        private readonly IUserManager UserManager;

        public UserController(IUserManager userManager)
        {
            UserManager = userManager;
        }

        [HttpGet]
        public List<Invitation> GetAllGroup([FromQuery] int userID)
        {
            return UserManager.GetAllGroup(userID);
        }
    }
}