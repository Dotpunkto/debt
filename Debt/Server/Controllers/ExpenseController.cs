using System.Collections.Generic;
using Debt.Models.Entities;
using Debt.Server.Models.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Server.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ExpenseController
    {
        private readonly IExpenseManager ExpenseManager;

        public ExpenseController(IExpenseManager expenseManager)
        {
            ExpenseManager = expenseManager;
        }

        [HttpPost]
        public void CreateExpense([FromBody] Expense expense)
        {
            ExpenseManager.CreateExpense(expense);
        }

        [HttpDelete]
        public void CancelExpense([FromQuery] int expenseID)
        {
            ExpenseManager.CancelExpense(expenseID);
        }

        [HttpGet]
        public ExpenseRequest GetGroupExpense([FromQuery] int groupID)
        {
            return ExpenseManager.GetAllGroupExpense(groupID);
        }

        [HttpPut]
        public void RefundExpense([FromQuery] int expenseID)
        {
            ExpenseManager.RefundExpense(expenseID);
        }

        /*[HttpGet]
        public List<User> GetAllExpense([FromQuery] int id)
        {
            return ExpenseManager.GetUserAndExpense(id);
        }*/

    }
}