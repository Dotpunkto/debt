﻿using Debt.Models.Entities;
using Debt.Server.Models.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Debt.Server.Controllers
{
    [Route("api/[controller]/[action]")]
    public class GroupController : Controller
    {
        private readonly IGroupManager GroupManager;

        public GroupController(IGroupManager groupManager)
        {
            GroupManager = groupManager;
        }

        [HttpPost]
        public void Invitation([FromBody] Invitation invitation)
        {
            GroupManager.Invitation(invitation);
        }

        [HttpPost]
        public void CreateGroup([FromQuery] string name)
        {
            GroupManager.CreateGroup(name);
        }

        [HttpPut]
        public void AcceptInvitation([FromQuery] int invitationID)
        {
            GroupManager.AccepteInvitation(invitationID);
        }
    }
}
