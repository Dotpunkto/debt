﻿using System.Collections.Generic;
using Debt.Models.Entities;
using Debt.Server.Models.Interfaces;

namespace Debt.Server.Models.Services
{
    public class UserManager : IUserManager
    {
        private readonly IUserRepository UserRepository;
        private readonly IGroupRepository GroupRepository;

        public UserManager(IUserRepository userRepository, IGroupRepository groupRepository)
        {
            UserRepository = userRepository;
            GroupRepository = groupRepository;
        }

        public User GetUserByID(int id)
        {
            return UserRepository.GetUserByID(id);
        }

        public List<Invitation> GetAllGroup(int userID)
        {
            return GroupRepository.GetGroupByUserID(userID);
        }
    }
}
