using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using Debt.Models.Entities;
using Debt.Server.Models.Interfaces;

namespace Debt.Server.Models.Services
{
    public class ExpenseManager : IExpenseManager
    {
        private readonly IExpenseRepository ExpenseRepository;
        private readonly IGroupRepository GroupRepository;

        public ExpenseManager(IExpenseRepository expenseRepository, IGroupRepository groupRepository)
        {
            ExpenseRepository = expenseRepository;
            GroupRepository = groupRepository;
        }

        public void CancelExpense(int expenseID)
        {
            ExpenseRepository.RemoveExpense(expenseID);
        }

        public void CreateExpense(Expense expense)
        {
            ExpenseRepository.CreateExpense(expense);
        }

        public ExpenseRequest GetAllGroupExpense(int id)
        {
            List<Expense> expenseList = ExpenseRepository.GetAllGroupExpense(id);
            float expenseTotal = 0;
            foreach (var expense in expenseList)
            {
                expenseTotal += expense.Amount;
            }
            ExpenseRequest expenseRequest = new ExpenseRequest
            {
                ExpenseTotal = expenseTotal,
                ExpensesList = expenseList
            };
            return expenseRequest;
        }

        public void RefundExpense(int expenseID)
        {
            Expense expense = ExpenseRepository.GetExpenseByID(expenseID);
            expense.Refund = true;
            ExpenseRepository.UpdateExpense(expense);
        }

        private List<User> GetUserEpense(int id)
        {
            List<Invitation> groups = GroupRepository.GetUsersByGroupID(id);
            List<User> users = new List<User>();
            foreach (var group in groups)
            {
                group.User.UserExpenses = ExpenseRepository.GetExpenseUser(id, group.UseID);
                users.Add(group.User);
            }
            return users;
        }

        public void GetUserAndExpense(int id)
        {
            float total = ExpenseRepository.GetTotalOfAmount(id);
            List<User> users = GetUserEpense(id);
            float debt = total / users.Count;


        }
    }
}