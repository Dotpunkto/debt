﻿using System.Collections.Generic;
using Debt.Models.Entities;
using Debt.Server.Models.Interfaces;

namespace Debt.Server.Models.Services
{
    public class GroupManager : IGroupManager
    {
        private readonly IGroupRepository GroupRepository;
        private readonly IUserManager UserManager;

        public GroupManager(IGroupRepository groupRepository, IUserManager userManager)
        {
            GroupRepository = groupRepository;
            UserManager = userManager;
        }

        public void CreateGroup(string name)
        {
            GroupRepository.AddGroup(name);
        }

        public void Invitation(Invitation invitation)
        {
            invitation.User = UserManager.GetUserByID(invitation.UseID);
            GroupRepository.Invitation(invitation);
        }

        public void AccepteInvitation(int id)
        {
            GroupRepository.AccepteInvitation(id);
        }
    }
}
