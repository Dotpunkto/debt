using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Debt.Models.Entities
{
    public class GroupUser
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [JsonIgnore]
        public List<Invitation> Invitations { get; set; }
        [JsonIgnore]
        public List<Expense> Expenses { get; set; }
    }
}
