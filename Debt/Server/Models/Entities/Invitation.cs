using System.ComponentModel.DataAnnotations;

namespace Debt.Models.Entities
{
    public class Invitation
    {
        public int ID { get; set; }
        [Required]
        public int GroupID { get; set; }
        [Required]
        public int UseID { get; set; }
        public bool Status { get; set; }
        public User User { get; set; }
        public GroupUser Group { get; set; }
    }
}