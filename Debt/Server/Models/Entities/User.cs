﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Debt.Models.Entities
{
    public class User
    {
        public int ID { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public List<Invitation> Invitations { get; set; }
        [JsonIgnore]
        public List<Expense> UserExpenses { get; set; }
    }
}
