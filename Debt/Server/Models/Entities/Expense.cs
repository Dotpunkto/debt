using System.Collections.Generic;

namespace Debt.Models.Entities
{
    public class Expense
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public float Amount { get; set; }
        public bool Refund { get; set; } = false;
        public int UserID { get; set; }
        public int GroupID { get; set; }
        public int? ExpenseID { get; set; }
        public GroupUser Group { get; set; }
        public List<Expense> ExpenseList { get; set; }

    }
}