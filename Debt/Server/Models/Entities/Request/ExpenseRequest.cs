using System.Collections.Generic;
namespace Debt.Models.Entities
{
    public class ExpenseRequest
    {
        public float ExpenseTotal { get; set; }
        public List<Expense> ExpensesList { get; set; }
    }
}