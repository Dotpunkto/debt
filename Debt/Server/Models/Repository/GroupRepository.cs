﻿using System;
using System.Collections.Generic;
using System.Linq;
using Debt.Models.DataAccess;
using Debt.Models.Entities;
using Debt.Server.Models.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Debt.Server.Models.Repository
{
    public class GroupRepository : IGroupRepository
    {
        private readonly DebtContext Context;

        public GroupRepository(DebtContext context)
        {
            Context = context;
        }

        public void AccepteInvitation(int id)
        {
            Invitation invitation = Context.Invitation.FirstOrDefault(i => i.ID == id);
            if (invitation != null)
            {
                invitation.Status = true;
                Context.Invitation.Update(invitation);
                Context.SaveChanges();
            }
        }

        public void AddGroup(string name)
        {
            GroupUser group = new GroupUser
            {
                Name = name
            };
            Context.Group.Add(group);
            Context.SaveChanges();
        }

        public GroupUser GetGroupByID(int id)
        {
            return Context.Group.Find(id);
        }

        public void Invitation(Invitation invitation)
        {
            invitation.Group = GetGroupByID(invitation.GroupID);
            invitation.Status = false;
            Context.Invitation.Add(invitation);
            Context.SaveChanges();
        }

        public List<Invitation> GetGroupByUserID(int userID)
        {
            return Context.Invitation.Include(m => m.Group).Where(i => i.UseID == userID).ToList();
        }

        public List<Invitation> GetUsersByGroupID(int id)
        {
            return Context.Invitation.Include(g => g.User).Where(g => g.GroupID == id).ToList();
        }

    }
}
