using Debt.Models.DataAccess;
using Debt.Models.Entities;
using Debt.Server.Models.Interfaces;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace Debt.Server.Models.Repository
{
    public class ExpenseRepository : IExpenseRepository
    {
        private readonly DebtContext Context;
        public ExpenseRepository(DebtContext context)
        {
            Context = context;
        }

        public void CreateExpense(Expense expense)
        {
            Context.Expense.Add(expense);
            Context.SaveChanges();
        }

        public Expense GetExpenseByID(int id)
        {
            return Context.Expense.Include(e => e.ExpenseList).Where(e => e.ID == id).First();
        }

        public void RemoveExpense(int expenseID)
        {
            Expense expense = GetExpenseByID(expenseID);
            Context.Expense.Remove(expense);
            Context.SaveChanges();
        }

        public List<Expense> GetAllGroupExpense(int id)
        {
            return Context.Expense.Include(e => e.ExpenseList).Where(e => e.GroupID == id && e.ExpenseID == null).ToList();
        }

        public void RefundExpense(Expense Expense)
        {
            Context.Expense.Update(Expense);
            Context.SaveChanges();
        }

        public void UpdateExpense(Expense expense)
        {
            Context.Expense.Update(expense);
            Context.SaveChanges();
        }

        public List<Expense> GetExpenseUser(int groupID, int userID)
        {
            return Context.Expense.Where(e => e.GroupID == groupID && e.UserID == userID).ToList();
        }

        public float GetTotalOfAmount(int id)
        {
            return Context.Expense.Where(e => e.Refund == false && e.GroupID == id).Sum(e => e.Amount);
        }

    }
}