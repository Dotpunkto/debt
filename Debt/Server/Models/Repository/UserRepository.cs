﻿using Debt.Models.DataAccess;
using Debt.Models.Entities;
using Debt.Server.Models.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Debt.Server.Models.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly DebtContext Context;

        public UserRepository(DebtContext context)
        {
            Context = context;
        }

        public User GetUserByID(int id)
        {
            return Context.User.Find(id);
        }
    }
}
