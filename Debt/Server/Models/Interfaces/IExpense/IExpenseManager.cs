using System.Collections.Generic;
using Debt.Models.Entities;

namespace Debt.Server.Models.Interfaces
{
    public interface IExpenseManager
    {
        void CreateExpense(Expense expense);
        void CancelExpense(int expenseID);
        ExpenseRequest GetAllGroupExpense(int id);
        void RefundExpense(int expenseID);
    }
}