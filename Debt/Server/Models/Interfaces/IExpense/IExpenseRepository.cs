using System.Collections.Generic;
using Debt.Models.Entities;

namespace Debt.Server.Models.Interfaces
{
    public interface IExpenseRepository
    {
        void CreateExpense(Expense expense);
        Expense GetExpenseByID(int id);
        void RemoveExpense(int expenseID);

        List<Expense> GetAllGroupExpense(int id);
        void RefundExpense(Expense Expense);
        void UpdateExpense(Expense expense);
        List<Expense> GetExpenseUser(int groupID, int userID);
        float GetTotalOfAmount(int id);
    }
}