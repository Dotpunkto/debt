﻿using System.Collections.Generic;
using Debt.Models.Entities;

namespace Debt.Server.Models.Interfaces
{
    public interface IUserManager
    {
        User GetUserByID(int id);
        List<Invitation> GetAllGroup(int userID);
    }
}
