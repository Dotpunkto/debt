﻿using Debt.Models.Entities;

namespace Debt.Server.Models.Interfaces
{
    public interface IUserRepository
    {
        User GetUserByID(int id);
    }
}
