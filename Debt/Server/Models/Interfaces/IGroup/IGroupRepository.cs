﻿using System.Collections.Generic;
using Debt.Models.Entities;

namespace Debt.Server.Models.Interfaces
{
    public interface IGroupRepository
    {
        void Invitation(Invitation invitation);
        GroupUser GetGroupByID(int id);
        void AddGroup(string name);
        void AccepteInvitation(int id);
        List<Invitation> GetGroupByUserID(int userID);
        List<Invitation> GetUsersByGroupID(int id);
    }
}
