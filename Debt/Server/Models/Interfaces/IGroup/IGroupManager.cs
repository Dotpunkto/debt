﻿using Debt.Models.Entities;

namespace Debt.Server.Models.Interfaces
{
    public interface IGroupManager
    {
        void Invitation(Invitation invitation);
        void CreateGroup(string name);
        void AccepteInvitation(int id);
    }
}
