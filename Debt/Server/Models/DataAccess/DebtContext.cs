using Debt.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Debt.Models.DataAccess
{
    public class DebtContext : DbContext
    {
        public DebtContext(DbContextOptions<DebtContext> options) : base(options)
        {
        }

        public DbSet<User> User { get; set; }
        public DbSet<GroupUser> Group { get; set; }
        public DbSet<Invitation> Invitation { get; set; }
        public DbSet<Expense> Expense { get; set; }
    }
}